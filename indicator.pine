//@version=2
// @todo how to add info to each inputs?
// @todo  Add nothing in the shorttitle so that it does not clutter UI
study(title="Ichimoku, SAR, TD, Leledc Exhausion,SQ Momentum, Kaufman AMA wave", shorttitle=" ", overlay=true)

sp = input (true, "Show Parabolic SAR")
spLines = input (false, "Show Parabolic SAR icons")

sL = input (true, "Show Leledec Exhausion")
sTD = input (true, "Show TD")
sCloud = input (false, "Show Ichimoku lines")
sRMO = input(true, "Show Rahul Mohindar Oscillator markers")

// general variables
colorLime = #006400
colorGreen= #00ff00
colorRed = #ff0000
colorMaroon =#8b0000
colorBlue =#0000ff
colorBlack = #000000
colorWhite = #ffffff
colorTenkanViolet = #9400D3
colorKijun = #fdd8a0
colorGray = #a9a9a9

// following Parabilic SAR code adapted from https://www.tradingview.com/script/X2mAbFxu-CM-Parabolic-SAR/
// ***** Parabolic SAR *****
// *************************
//Created By ChrisMoody on 7/25/2014
//Simply Enhances Default Parabolic SAR by creating Two Color Options, One for UpTrend, Other for DownTrend
//Ability To Turn On/Off The Up Trending Parabolic SAR, And The Down Trending Parabolic SAR
start = input(2, minval=0, maxval=10, title="Start - Default = 2 - Multiplied by .01")
increment = input(2, minval=0, maxval=10, title="Step Setting (Sensitivity) - Default = 2 - Multiplied by .01" )
maximum = input(2, minval=1, maxval=10, title="Maximum Step (Sensitivity) - Default = 2 - Multiplied by .10")
sus = input(true, "Show Up Trending Parabolic Sar")
sds = input(true, "Show Down Trending Parabolic Sar")
disc = input(false, title="Start and Step settings are *.01 so 2 = .02 etc, Maximum Step is *.10 so 2 = .2")
//"------Step Setting Definition------"
//"A higher step moves SAR closer to the price action, which makes a reversal more likely."
//"The indicator will reverse too often if the step is set too high."

//"------Maximum Step Definition-----")
//"The sensitivity of the indicator can also be adjusted using the Maximum Step."
//"While the Maximum Step can influence sensitivity, the Step carries more weight"
//"because it sets the incremental rate-of-increase as the trend develops"

startCalc = start * .01
incrementCalc = increment * .01
maximumCalc = maximum * .10

sarUp = sar(startCalc, incrementCalc, maximumCalc)
sarDown = sar(startCalc, incrementCalc, maximumCalc)

colUp = spLines and close >= sarDown ? colorLime : na
colDown = spLines and close <= sarUp ? colorRed : na

plot(sp and sus and sarUp ? sarUp : na, title="Down Trending SAR", style=circles, linewidth=3,color=colUp)
plot(sp and sds and sarDown ? sarDown : na, title="Up Trending SAR", style=circles, linewidth=3,color=colDown)

// Following Leledc Exhausion Bar copied from https://www.tradingview.com/script/2rZDPyaC-Leledc-Exhaustion-Bar
// ***** Leledc Exhaustion Bar *****
// *********************************
maj_qual=6
maj_len=30
min_qual=5
min_len=5

maj=input(true,title="Show Major")
min=input(false,title="Show Minor")

lele(qual,len)=>
    bindex=nz(bindex[1],0)
    sindex=nz(sindex[1],0)
    ret=0
    if (close>close[4]) 
        bindex:=bindex + 1
    if(close<close[4]) 
        sindex:=sindex + 1
    if (bindex>qual) and (close<open) and high>=highest(high,len) 
        bindex:=0
        ret:=-1
    if ((sindex>qual) and (close>open) and (low<= lowest(low,len)))
        sindex:=0
        ret:=1
    return=ret

major=lele(maj_qual,maj_len)
minor=lele(min_qual,min_len)

// Plot the major signals 
plotchar(sL and maj ? (major==-1?high:na) : na, text="Leldc", char='↓' , location=location.abovebar,color=colorRed,transp=0,size=size.small)
plotchar(sL and maj ? (major==1?low:na) : na  ,  text="Leldc", char='↑', location=location.belowbar,color=colorGreen,transp=0,size=size.small)

// Plot the minor signals
plotchar(sL and min ? (minor==-1?high:na) : na, char='○',location=location.absolute,color=colorMaroon,transp=0,size=size.normal)
plotchar(sL and min ? (minor==1?low:na) : na , char='○',location=location.absolute,color=colorLime,transp=0,size=size.normal) 


// following code is the Ichimoku that has been adapted and always shown
// ***** Ichimoku *****
// ********************
tenkanPeriods = input(20, minval=1, title="Tenkan")
kijunPeriods = input(60, minval=1, title="Kijun")
chikouPeriods = input(120, minval=1, title="Chikou")
displacement = input(30, minval=1, title="Offset")

donchian(len) => avg(lowest(len), highest(len))

tenkan = donchian(tenkanPeriods)
kijun = donchian(kijunPeriods)
senkouA = avg(tenkan, kijun)
senkouB = donchian(chikouPeriods)
displacedSenkouA = senkouA[displacement]
displacedSenkouB = senkouB[displacement] 

bullishSignal = crossover(tenkan, kijun)
bearishSignal = crossunder(tenkan, kijun)

bullishSignalValues = iff(bullishSignal, tenkan, na)
bearishSignalValues = iff(bearishSignal, tenkan, na)

strongBullishSignal = bullishSignalValues > displacedSenkouA and bullishSignalValues > displacedSenkouB
neutralBullishSignal = (bullishSignalValues > displacedSenkouA and bullishSignalValues < displacedSenkouB) or (bullishSignalValues < displacedSenkouA and bullishSignalValues > displacedSenkouB)
weakBullishSignal = bullishSignalValues < displacedSenkouA and bullishSignalValues < displacedSenkouB

strongBearishSignal = bearishSignalValues < displacedSenkouA and bearishSignalValues < displacedSenkouB
neutralBearishSignal = (bearishSignalValues > displacedSenkouA and bearishSignalValues < displacedSenkouB) or (bearishSignalValues < displacedSenkouA and bearishSignalValues > displacedSenkouB)
weakBearishSignal = bearishSignalValues > displacedSenkouA and bearishSignalValues > displacedSenkouB

// Allow users to show or hide not strong signals. Always show strong signals
plotshape(neutralBullishSignal, style=shape.flag, size=size.small, location=location.abovebar, title="Neutral Bullish Signals", color=colorLime)
plotshape(weakBullishSignal, style=shape.xcross, size=size.tiny, location=location.abovebar, title="Weak Bullish Signals", color=colorLime)

plotshape(neutralBearishSignal, style=shape.flag, size=size.small, location=location.belowbar, title="Neutral Bearish Signals", color=colorMaroon)
plotshape(weakBearishSignal, style=shape.xcross, size=size.tiny, location=location.belowbar, title="Weak Bearish Signals", color=colorMaroon)

// plotchar(strongBullishSignal,location=location.abovebar, text="Bullish Ichimoku", color=colorGreen)
// plotchar(strongBearishSignal,location=location.belowbar, text="Bearish Ichimoku", color=colorRed)

plotchar(strongBearishSignal, text="Ichimoku", char='↓' , location=location.abovebar,color=colorRed,transp=0,size=size.small)
plotchar(strongBullishSignal,  text="Ichimoku", char='↑', location=location.belowbar,color=colorGreen,transp=0,size=size.small)

plot(tenkan, color=iff(sCloud, colorTenkanViolet, na), title="Tenkan", linewidth=2)
plot(kijun, color=iff(sCloud, colorKijun, na), title="Kijun", linewidth=2)

plot(close, offset = -displacement, color=iff(sCloud, colorLime, na), title="Chikou", linewidth=1)
p1 = plot(senkouA, offset=displacement, color=colorGreen, title="Senkou A", linewidth=3)
p2 = plot(senkouB, offset=displacement, color=colorRed, title="Senkou B", linewidth=3)
fill(p1, p2, color = senkouA > senkouB ? colorGreen : colorRed)  
 
// Following SQ MOM adapted from Lazy Bear
// ***** Squeeze Momentum *****
// ****************************
length = input(20, title="BB Length")
mult = input(2.0,title="BB MultFactor")
lengthKC=input(20, title="KC Length")
multKC = input(1.5, title="KC MultFactor")

useTrueRange = input(true, title="Use TrueRange (KC)", type=bool)

// Calculate BB
source = close
basis = sma(source, length)
dev = mult * stdev(source, length)
upperBB = basis + dev
lowerBB = basis - dev

// Calculate KC
ma = sma(source, lengthKC)
range = useTrueRange ? tr : (high - low)
rangema = sma(range, lengthKC)
upperKC = ma + rangema * multKC
lowerKC = ma - rangema * multKC

sqzOn  = (lowerBB > lowerKC) and (upperBB < upperKC)
sqzOff = (lowerBB < lowerKC) and (upperBB > upperKC)
noSqz  = (sqzOn == false) and (sqzOff == false)

val = linreg(source - avg(avg(highest(high, lengthKC), lowest(low, lengthKC)),sma(close,lengthKC)), lengthKC,0)

bcolor = iff( val > 0, iff( val > nz(val[1]), colorLime, colorGreen), iff( val < nz(val[1]), colorRed, colorMaroon))
scolor = noSqz ? blue : sqzOn ? colorBlack : colorGray 

//standard condition
longCondition = scolor[1]!=colorGray and scolor==colorGray and bcolor==colorLime and scolor[1]==colorBlack
exitLongCondition = bcolor==colorGreen and bcolor[1]==colorLime
shortCondition = scolor[1]!=colorGray and scolor==colorGray and bcolor==colorRed and scolor[1]==colorBlack
exitShortCondition = bcolor==colorMaroon and bcolor[1]==colorRed

plotchar(shortCondition, text="SqzS", char='↓' , location=location.abovebar,color=colorRed,transp=0,size=size.small)
plotchar(longCondition, text="SqzL", char='↑' , location=location.belowbar,color=colorGreen,transp=0,size=size.small)

plotchar(exitLongCondition,  text="", char='X', location=location.belowbar,color=colorGreen,transp=0,size=size.small)
plotchar(exitShortCondition,  text="", char='X', location=location.abovebar,color=colorRed,transp=0,size=size.small)

// @todo plotchar exit condition but once only until next reverse condition is shown
// plotshape(exitLongCondition, color=#006400, style=shape.arrowdown, text="Exit Buy (SQ MOM)", location = location.top)
// plotshape(exitShortCondition, color=#006400, style=shape.arrowup, text="Exit Short (SQ MOM)", location = location.bottom)


// Following is adapted from Kaufman AMA wave from Lazy Bear 
// ***** Kaufman AMA wave *****
// ****************************
src=close
lengthAMA=input(20)
filterp = input(10)

d=abs(src-src[1])
s=abs(src-src[lengthAMA])
noise=sum(d, lengthAMA)
efratio=s/noise
fastsc=0.6022
slowsc=0.0645 

smooth=pow(efratio*fastsc+slowsc, 2)
ama=nz(ama[1], close)+smooth*(src-nz(ama[1], close))
filter=filterp/100 * stdev(ama-nz(ama), lengthAMA)
amalow=ama < nz(ama[1]) ? ama : nz(amalow[1])
amahigh=ama > nz(ama[1]) ? ama : nz(amahigh[1])
bw=(ama-amalow) > filter ? 1 : (amahigh-ama > filter ? -1 : 0)
s_color=bw > 0 ? colorGreen : (bw < 0) ? colorRed : colorBlue

amaLongConditionEntry = s_color==colorGreen and s_color[1]!=colorGreen
amaShortConditionEntry = s_color==colorRed and s_color[1]!=colorRed
plotchar(sRMO and amaLongConditionEntry,  text="AMA", char='↑', location=location.bottom,color=colorGreen,transp=0,size=size.small)
plotchar(sRMO and amaShortConditionEntry,  text="AMA", char='↓', location=location.above,color=colorRed,transp=0,size=size.small)


// Following is adapted from Rahul Mohindar Oscillator https://www.tradingview.com/script/efHJsedw-Indicator-Rahul-Mohindar-Oscillator-RMO/
// ***** Rahul Mohindar Oscillator *****
// *************************************
C=close
cm2(x) => sma(x,2)
ma1=cm2(C)
ma2=cm2(ma1)
ma3=cm2(ma2)
ma4=cm2(ma3)
ma5=cm2(ma4)
ma6=cm2(ma5)
ma7=cm2(ma6)
ma8=cm2(ma7)
ma9=cm2(ma8)
ma10=cm2(ma9)
SwingTrd1 = 100 * (close - (ma1+ma2+ma3+ma4+ma5+ma6+ma7+ma8+ma9+ma10)/10)/(highest(C,10)-lowest(C,10))
SwingTrd2=ema(SwingTrd1,30)
SwingTrd3=ema(SwingTrd2,30)
RMO= ema(SwingTrd1,81)
Buy=cross(SwingTrd2,SwingTrd3)
Sell=cross(SwingTrd3,SwingTrd2)
Bull_Trend=ema(SwingTrd1,81)>0
Bear_Trend=ema(SwingTrd1,81)<0
Ribbon_kol=Bull_Trend ? colorGreen : (Bear_Trend ? colorRed : colorBlue)
Impulse_UP= SwingTrd2 > 0
Impulse_Down= RMO < 0
bar_kol=Impulse_UP ? colorGreen : (Impulse_Down ? colorRed : (Bull_Trend ?  colorGreen : colorBlue))
rahulMohindarOscilllatorLongEntry = Ribbon_kol==colorGreen and Ribbon_kol[1]!=colorGreen and Ribbon_kol[1]==colorRed and bar_kol==colorGreen
rahulMohindarOscilllatorShortEntry = Ribbon_kol==colorRed and Ribbon_kol[1]!=colorRed and Ribbon_kol[1]==colorGreen and bar_kol==colorRed

// Need to start putting markes not near bars as it is getting crowded
plotchar(rahulMohindarOscilllatorLongEntry,  text="RMO", char='↑', location=location.bottom,color=colorGreen,transp=0,size=size.small)
plotchar(rahulMohindarOscilllatorShortEntry,  text="RMO", char='↓', location=location.top,color=colorRed,transp=0,size=size.small)


// following TD Sequential code adapted from https://www.tradingview.com/script/nUtNecpf-TD-Sequential/
// the TD sequenutial either not counting properly or the indicator has bad success rate.
// ***** TD Sequential *****
// *************************
transp=0 // input(0)
Numbers=input(false)
SR=input(false)
Barcolor=input(true)

TD = close > close[4] ?nz(TD[1])+1:0
TS = close < close[4] ?nz(TS[1])+1:0

TDUp = TD - valuewhen(TD < TD[1], TD , 1 )
TDDn = TS - valuewhen(TS < TS[1], TS , 1 )
plotshape(Numbers?(TDUp==1?true:na):na,style=shape.triangledown,text="1",color=green,location=location.abovebar,transp=transp)
plotshape(Numbers?(TDUp==2?true:na):na,style=shape.triangledown,text="2",color=green,location=location.abovebar,transp=transp)
plotshape(Numbers?(TDUp==3?true:na):na,style=shape.triangledown,text="3",color=green,location=location.abovebar,transp=transp)
plotshape(Numbers?(TDUp==4?true:na):na,style=shape.triangledown,text="4",color=green,location=location.abovebar,transp=transp)
plotshape(Numbers?(TDUp==5?true:na):na,style=shape.triangledown,text="5",color=green,location=location.abovebar,transp=transp)
plotshape(Numbers?(TDUp==6?true:na):na,style=shape.triangledown,text="6",color=green,location=location.abovebar,transp=transp)
plotshape(Numbers?(TDUp==7?true:na):na,style=shape.triangledown,text="7",color=green,location=location.abovebar,transp=transp)
plotshape(Numbers?(TDUp==8?true:na):na,style=shape.triangledown,text="8",color=green,location=location.abovebar,transp=transp)
plotshape(Numbers?(TDUp==9?true:na):na,style=shape.triangledown,text="9",color=green,location=location.abovebar,transp=transp)

plotshape(Numbers?(TDDn==1?true:na):na,style=shape.triangleup,text="1",color=red,location=location.belowbar,transp=transp)
plotshape(Numbers?(TDDn==2?true:na):na,style=shape.triangleup,text="2",color=red,location=location.belowbar,transp=transp)
plotshape(Numbers?(TDDn==3?true:na):na,style=shape.triangleup,text="3",color=red,location=location.belowbar,transp=transp)
plotshape(Numbers?(TDDn==4?true:na):na,style=shape.triangleup,text="4",color=red,location=location.belowbar,transp=transp)
plotshape(Numbers?(TDDn==5?true:na):na,style=shape.triangleup,text="5",color=red,location=location.belowbar,transp=transp)
plotshape(Numbers?(TDDn==6?true:na):na,style=shape.triangleup,text="6",color=red,location=location.belowbar,transp=transp)
plotshape(Numbers?(TDDn==7?true:na):na,style=shape.triangleup,text="7",color=red,location=location.belowbar,transp=transp)
plotshape(Numbers?(TDDn==8?true:na):na,style=shape.triangleup,text="8",color=red,location=location.belowbar,transp=transp)
plotshape(Numbers?(TDDn==9?true:na):na,style=shape.triangleup,text="9",color=red,location=location.belowbar,transp=transp)


// S/R Code By johan.gradin
//------------//
// Sell Setup //
//------------//
priceflip = barssince(close<close[4])
sellsetup = close>close[4] and priceflip
sell = sellsetup and barssince(priceflip!=9)
sellovershoot = sellsetup and barssince(priceflip!=13)
sellovershoot1 = sellsetup and barssince(priceflip!=14)
sellovershoot2 = sellsetup and barssince(priceflip!=15)
sellovershoot3 = sellsetup and barssince(priceflip!=16)

//----------//
// Buy setup//
//----------//
priceflip1 = barssince(close>close[4])
buysetup = close<close[4] and priceflip1
buy = buysetup and barssince(priceflip1!=9)
buyovershoot = barssince(priceflip1!=13) and buysetup
buyovershoot1 = barssince(priceflip1!=14) and buysetup
buyovershoot2 = barssince(priceflip1!=15) and buysetup
buyovershoot3 = barssince(priceflip1!=16) and buysetup

//----------//
// TD lines //
//----------//
TDbuyh = valuewhen(buy,high,0)
TDbuyl = valuewhen(buy,low,0)
TDsellh = valuewhen(sell,high,0)
TDselll = valuewhen(sell,low,0)

//----------//
//   Plots  //
//----------//
// @Fixme fix the colors
plot(sTD and SR?(TDbuyh ? TDbuyl: na):na,style=circles, linewidth=1, color=red)
plot(sTD and SR?(TDselll ? TDsellh : na):na,style=circles, linewidth=1, color=lime)
barcolor(sTD and Barcolor?(sell? #FF0000 : buy? #00FF00 : sellovershoot? #FF66A3 : sellovershoot1? #FF3385 : sellovershoot2? #FF0066 : sellovershoot3? #CC0052 : buyovershoot? #D6FF5C : buyovershoot1? #D1FF47 : buyovershoot2? #B8E62E : buyovershoot3? #8FB224 : na):na)


// @TODO combine above three signals into one signal
// @fixme Can not combine Leledec for obvious reasons. It signals exhausion or possible end of trend.
plotshape(strongBullishSignal and sp and sus and sarUp ? sarUp : na, color=colorGreen, style=shape.arrowup, text="Buy", location = location.abovebar)

// Adds alert condition when the above signal comes up
alertcondition(strongBullishSignal and sp and sus and sarUp ? sarUp : na, title='Parabolid SAR and Cloud both Bullish', message='Parabolid SAR and Cloud both Bullish')
